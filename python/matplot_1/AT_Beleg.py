import numpy as np
import matplotlib.pyplot as plt
import control

# Abgeleitete Parameter Nicolas
GBP = 1500e3
g_v0 = 1090
R_OV = 50
C_0 = 47e-6
R_esr = 0.94444444

# Abgeleitete Parameter Christina
#GBP = 1350e3
#g_v0 = 1210
#R_OV = 50
#C_0 = 47e-6
#R_esr = 0.833333333333

# Abgeleitete Parameter Hannes
#GBP = 98415e1
#g_v0 = 1240
#R_OV = 47
#C_0 = 100e-6
#R_esr = 0.5

# Verstärker
G_V_num = [g_v0]
G_V_den = [np.sqrt(g_v0**2 - 1)/(2*np.pi*GBP), 1]

G_V = control.tf(G_V_num, G_V_den)

# Längstransistor
g_p0 = 20
C_par = 4.9e-9

G_tran = control.tf([g_p0], [C_par * R_OV, 1])
G = control.series(G_V, G_tran)

# Last mit Ausgangskondensator
R_L = 5

G_last = control.tf([R_esr*C_0*R_L, R_L], [R_L*C_0, 1])

G = control.series(G, G_last)

# Spannungsteiler
R_1 = 30e3
R_2 = 10e3

G_teiler = control.tf([R_2],[R_1 + R_2])

G = control.series(G, G_teiler)

# Optional: Kompletter Regelkreis

#G = control.feedback(G, 1, sign=-1)

plt.figure(figsize=(10, 6))
control.bode_plot(G, dB=True)
plt.show()