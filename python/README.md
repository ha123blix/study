# Python als Matlab Ersatz #

## IDE ##

JetBrains Py Charm

+ https://www.jetbrains.com/pycharm/

## Pakete ##

### import numpy as np ###

Tutorial:

+ https://docs.scipy.org/doc/numpy/user/quickstart.html
+ https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html


### import matplotlib.pyplot as plt ###

+ https://matplotlib.org/


### import control ###

LTI system representation:

+ https://python-control.readthedocs.io/en/latest/conventions.html

## Bodediagramm ##

Tutorial:

+ http://blog.analogmachine.org/2015/02/07/bode-plots-using-python/




## anderes ##


asciidoc documantation:
https://asciidoctor.org/docs/asciidoc-writers-guide/
