# Use Cases

Create a use case diagram (or a simple list of use cases) for the new functionality.

MIN-95


| Number    | Name                           | Tickets | Description                                                                       | Access Dadabase | Nofify ListAdapter | Access Communication | AccessPrinter | AccessInternet |
|-----------|--------------------------------|---------|-----------------------------------------------------------------------------------|-----------------|--------------------|----------------------|---------------|----------------|
| 1-1       | AddEvent                       |         |                                                                                   | w               |                    |                      |               |                |
| 1-2       | OpenEvent                      |         |                                                                                   | r               |                    |                      |               |                |
| 1-3       | DeleteEvent                    |         |                                                                                   | w               |                    |                      |               |                |
| 2-1       | RegisterCompetitorUI           |         | Register a Competitor by clicking on Add on UI and typing in all the data.        | w               |                    |                      |               |                |
| 2-2       | ReadCardFast                   |         | listening for Cards. Reading only personal data                                   |                 |                    | yes                  |               |                |
| 2-3       | RegisterCompetitorFromCardFast |         | Register a Competitor by data from ReadCardFast                                   | w               |                    |                      |               |                |
| 2-4       | ReadCardFull                   |         | listening for Cards. Reaning all data                                             |                 |                    | yes                  |               |                |
| 2-5       | RegisterCompetitorFromCardFull |         | If competitor is not registerd, Register a Competitor by data from ReadCardFull   | w               |                    |                      |               |                |
| 2-6       | EditCompetitorFromCardFull     |         | If competitor is registerd, Add readed punch data from ReadCardFull zu Competitor | w               |                    |                      |               |                |
| 2-7       | EditCompetitorUI               |         |                                                                                   | w               |                    |                      |               |                |
| 2-8       | DeleteCompetitior              |         |                                                                                   | w               |                    |                      |               |                |
| 2-9       | UploadCompetitor               |         |                                                                                   |                 |                    |                      |               | yes            |
| 2-10      | PrintCompetitor                |         |                                                                                   |                 |                    |                      | yes           |                |
| 3-1       | DisplayResults                 |         | Displaying the results for all CalculateCourseResults                             |                 |                    |                      |               |                |
| 3-2       | PrintResults                   |         |                                                                                   |                 |                    |                      | yes           |                |
| 1-4       | SaveEventSettings              |         |                                                                                   | w               |                    |                      |               |                |
| 4-1       | AddCourse                      |         |                                                                                   | w               |                    |                      |               |                |
| 4-2       | EditCourse                     |         |                                                                                   | w               |                    |                      |               |                |
| 4-3       | DeleteCourse                   |         |                                                                                   | w               |                    |                      |               |                |
| 4-4       | CalculateCourseResults         |         | Calculates the results for the whole course                                       |                 |                    |                      |               |                |
   