# Activities for event administration

With concept from MIN-55.

Overview of all new activities and other layout files.


Caution:

*   Resource names are not aware of case sensitive and must not contain: '-' and space.

*   The whole screen (visible and invisible parts) are set up by several layouts and layout files that are inflated in other layouts or in one or more activities.
    Each resource id that is inflated on the scren must not be represented twice or more!
    Therefore it is nessesary that each ui element has a unique resource id or no android:id attribute at all!

*   In resources elements have to be declared before used. 


To guarantee each ui element (Button, EditText, etc.) has a unique id we follow these rules:

*   Ui elements that are not used in the code (like labels that do not change their text) have no id.

    _android:id_ will be missing.


*   Others: 

    _android:id="@+id/__[uniqueId]__"_

    __uniqueId__ = resource shortcut + ui element type + name

    example: _android:id="@+id/aecomp_et_name"_






## AEventDisplay

Entry page for event.
Displays in ViewPager 3 Fragments for registerd runners, readed runners and results.

resouce name: activity_eventDisplay
resource shortcut: aed

### FragmentRegistration

resouce name: fragment_registration
resource shortcut: freg

### FragmentReadout

resouce name: fragment_readout
resource shortcut: fread

### FragmentResult

resouce name: fragment_result
resource shortcut: fres


## AEventAdministration

Event settings.
Displays in ViewPager 3 Fragments for event settings, courses and logging.

resouce name: activity_eventAdministration
resource shortcut: aea

### FragmentEvent

resouce name: fragment_event
resouce shortcut: fevent

### FragmentCourses

resouce name: fragment_courses
resouce shortcut: fcour


### FragmentService

resouce name: fragment_service
resouce shortcut: fser


## AEditCompetitor

Adding and Editing Competitor (hide / show some parts)

resouce name: activity_editCompetitor
resouce shortcut: aecomp


## AEditCourse

Adding and Editing Course (hide / show some parts)

resouce name: activity_editCourse
resouce shortcut: aecour

