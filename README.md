# STUDY #

TU Dresden
https://tu-dresden.de/studium/im-studium/studienstart

### Nicht wöchentliche Vorlesungen:###
    (1/2) ungerade KW
    (2/2) gerade KW


# TU Dresden, Informationssystemtechnik (Diplom)

### Links Hilfe Informationssystemtechnik

* Netaction, Forum
https://et.netaction.de/forum/index.php

* nextcloud, Cloud
https://cloud.ketograph.de/index.php/s/CaNycdAUG7P5Hzp?path=%2FGrundstudium

* Informatik dropbox, Cloud
https://www.dropbox.com/sh/eu8bza419il6m8h/AABMiwR4Rtb7CTDW3OA0aARqa/Faecher?dl=0

* unilinks, Informationssytemtechnik seite
https://jklmnn.github.io/unilinks/#ist


### Links TU Dresden

* **Informationen zum Prüfungsverfahren** https://tu-dresden.de/ing/elektrotechnik/studium/studieren-an-der-fakultaet/pruefungen

* OPAL
* HISQUIS
* jExam
* LSKonline
* Selma

Weitere: (nicht so wichtig)

* Acribit copyshop, SLUB
https://www.indiprint.de/de

* Campussachesen office 365
https://campussachsen.tu-dresden.de/o365/login.php


# Semester WS/2018 #

### Zu wiederholende Prüfungen ###

* Mathe III https://tu-dresden.de/mn/math/wir/studium/lehrveranstaltungen/mathematik-fuer-et/mathe3
  Übungstermine: http://www.math.tu-dresden.de/%7Efeldm/mathe3_WS1819/Tutor-Uebung.pdf
* Dynamische Netzwerke https://tu-dresden.de/ing/elektrotechnik/iee/ge/studium/lehrveranstaltungen/et3
* (Mathe IV https://tu-dresden.de/mn/math/stochastik/das-institut/beschaeftigte/wiltrud-kuhlisch/lehre/et_4_so_2018)


### Fächer ###

* Betriebssysteme http://www.inf.tu-dresden.de/index.php?node_id=1312&ln=de
  InfoDropbox: https://www.dropbox.com/sh/eu8bza419il6m8h/AAB5pv_pfUf1PXxbFU4INFina/Faecher/Betriebssysteme%20und%20Sicherheit?dl=0&subfolder_nav_tracking=1
* Schaltkreis und Systementwicklung https://tu-dresden.de/ing/elektrotechnik/iee/hpsn/studium/lehrveranstaltungen/sse
* * NA (nicht viel): https://et.netaction.de/forum/viewforum.php?f=220
* * Tipps Tricks: https://tu-dresden.de/ing/elektrotechnik/iee/hpsn/studium/materialien/info
* Formale Systeme https://iccl.inf.tu-dresden.de/web/Formale_Systeme(WS2018)
  NA: https://et.netaction.de/forum/viewforum.php?f=291
  InfoDropbox: https://www.dropbox.com/sh/eu8bza419il6m8h/AAAn_NBmAwJnBM-V5apV3A_la/Faecher/Formale%20Systeme?dl=0&subfolder_nav_tracking=1
* Signalverarbeitung https://tu-dresden.de/ing/elektrotechnik/ias/juniorprofessur-fuer-kognitive-systeme/studium/lehrveranstaltungen/signalverarbeitung
  NA: https://et.netaction.de/forum/viewforum.php?f=153


### AQUA ###

* Aqua informationen IST: https://tu-dresden.de/ing/elektrotechnik/studium/studieren-an-der-fakultaet/lehrveranstaltungen/aqua

### Frei ###

    Lehrveranstaltungen:
    Mo, 08.10.2018 bis Fr, 21.12.2018 sowie Mo, 07.01.2019 bis Sa, 02.02.2019
    
    Vorlesungsfreie Zeiten/Feiertage:
    Reformationstag: Mi, 31.10.2018
    Buß- und Bettag: Mi, 21.11.2018
    Jahreswechsel: Sa, 22.12.2018 bis So, 06.01.2019
    Vorlesungsfreie Zeit: Mo, 04.02.2019 bis So, 31.03.2019
    Kernprüfungszeit: Mo, 04.02.2019 bis Sa, 02.03.2019

 



# Semester SS/2018 #

09.04.2018 bis 18.05.2018 sowie 28.05.2018 bis 21.07.2018
Vorlesungsfreie Zeiten/Feiertage:



    Ostern: 30.03.2018 bis 02.04.2018
    1. Mai: Di, 01.05.2018
    Himmelfahrt: Do, 10.05.2018
    Pfingsten: Sa, 19.05.2018 bis So, 27.05.2018
    Dies academicus: Mi, 06.06.2018
    Vorlesungsfreie Zeit: Mo, 23.07.2018 bis So, 30.09.2018
    Kernpr�fungszeit: Mo, 23.07.2018 bis Sa, 18.08.2018
    

### links Veranstaltungen SS2018 ###

Mathe 4
https://tu-dresden.de/mn/math/stochastik/das-institut/beschaeftigte/wiltrud-kuhlisch/lehre/et_4_so_2018

Automatisierungstechnik
http://www.et.tu-dresden.de/ifa/index.php?id=632

 -Automatisierungstechnik im OPAL
https://bildungsportal.sachsen.de/opal/auth/RepositoryEntry/16864575515/CourseNode/95402687772152

Schaltungstechnik
https://tu-dresden.de/ing/elektrotechnik/iee/ccn/studium/lehrveranstaltungen/schaltungstechnik?set_language=de

Systemtheorie
https://tu-dresden.de/ing/elektrotechnik/ifn/tnt/studium/lv/systemtheorie


Rechnerarchitektur 2
https://tu-dresden.de/ing/informatik/ti/professur-fuer-rechnerarchitektur/studium/lehrveranstaltungen/vorlesungen/vorlesung-rechnerarchitektur-ii


Elektrotechnik Praktikum
https://tu-dresden.de/ing/elektrotechnik/iee/ge/studium/lehrveranstaltungen/praktikum-et


TGI Praktikum (Hardware Praktikum)
https://tu-dresden.de/ing/informatik/ti/ads/studium/summer/hwp




### timetable ###
Stundenplan:

![Scheme](https://bitbucket.org/ha123blix/study/raw/master/images/timetable.png)




# anderes


asciidoc documantation:
https://asciidoctor.org/docs/asciidoc-writers-guide/
